<?php
echo 'Switch és match';


$mark = rand(1, 10);
echo '<br>';
echo $mark;
echo '<br>';
switch($mark) {
    case 0:
    case 1:
        echo 'Sikertelen vizsga!';
        break;
    case 2:
    case 3:
    case 4:
        echo 'Sikeres vizsga!';
        break;
    case 5:
        echo "Sikeres vizsga dícsérettel!";
        break;
    default:
        echo "Ismeretlen vizsgaeredmény!";
        break;
};

match ($mark) {
    0, 1 => print 'Sikertelen vizsga!',
    2, 3, 4 => print 'Sikeres vizsga!',
    5 => print 'Sikeres vizsga dícsérettel!',
    default => print "Ismeretlen vizsgaeredmény!"
};

